<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>About_Dialog</name>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="80"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="253"/>
        <source>License</source>
        <translation>Povolení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="86"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2012-2020 Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is free software: you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Free Software Foundation, either version 3 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with this program.  If not, see &amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Autorské právo (C) 2012  Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter je svobodným softwarem; můžete jej rozdávat a/nebo&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;upravovat za podmínek GNU General Public License (GPL), jak jsou zveřejněny&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Free Software Foundation, buď ve verzi 3 licence, nebo&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(podle své volby) v kterékoli pozdější verzi.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter je šířen v naději, že bude užitečný,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;ale BEZ JAKÉKOLI ZÁRUKY; také bez předpokládané záruky&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PRODEJNOSTI nebo POUŽITELNOSTI PRO NĚJAKÝ URČITÝ ÚČEL.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Více podrobností naleznete v GNU Library General Public License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Kopii GNU Library General Public License byste měl obdržet&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;společně s tímto program.  Pokud ne, podívejte se na 
&amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="115"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="258"/>
        <source>System</source>
        <translation>Systém</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="151"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="254"/>
        <source>Data directories:</source>
        <translation>Adresáře s daty:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="165"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="255"/>
        <source>Settings file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="196"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="257"/>
        <source>Writable data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings location:</source>
        <translation type="obsolete">Umístění nastavení:</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Přídavné moduly</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="182"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="256"/>
        <source>Plugins are loaded from the following directories:</source>
        <translation>Pří&amp;davné moduly jsou nahrány z následujících adresářů:</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2012-2020 Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is free software: you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Free Software Foundation, either version 3 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with this program.  If not, see &amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Autorské právo (C) 2012  Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is free software: je svobodným softwarem; můžete jej rozdávat a/nebo upravovat&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;za podmínek GNU General Public License (GPL), jak jsou zveřejněny&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Free Software Foundation, buď ve verzi 3 licence, nebo&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(podle své volby) v kterékoli pozdější verzi.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter je šířen v naději, že bude užitečný,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;ale BEZ JAKÉKOLI ZÁRUKY; také bez předpokládané záruky&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PRODEJNOSTI nebo POUŽITELNOSTI PRO NĚJAKÝ URČITÝ ÚČEL.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Více podrobností naleznete v GNU Library General Public License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Kopii GNU Library General Public License byste měl obdržet&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;společně s tímto program.  Pokud ne, podívejte se na &amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="223"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="259"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="42"/>
        <location filename="../../src/dialogs/about_dialog.cpp" line="65"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="103"/>
        <source>Copy directory name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="104"/>
        <source>Open in file manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="111"/>
        <source>Copy file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="112"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_about_dialog.h" line="231"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2012-2014  Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is free software: you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Free Software Foundation, either version 3 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with this program.  If not, see &amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AbstractWidgetList</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="67"/>
        <source>Add New</source>
        <translation type="unfinished">Přidat nový</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="118"/>
        <source>Move Up</source>
        <translation type="unfinished">Posunout nahoru</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="119"/>
        <source>Move Down</source>
        <translation type="unfinished">Posunout dolů</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="120"/>
        <source>Remove</source>
        <translation type="unfinished">Odstranit</translation>
    </message>
</context>
<context>
    <name>Abstract_Widget_List</name>
    <message>
        <source>Add New</source>
        <translation type="vanished">Přidat nový</translation>
    </message>
    <message>
        <source>Move Up</source>
        <translation type="vanished">Posunout nahoru</translation>
    </message>
    <message>
        <source>Move Down</source>
        <translation type="vanished">Posunout dolů</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Odstranit</translation>
    </message>
</context>
<context>
    <name>Application_Info</name>
    <message>
        <location filename="../../src/application_info.cpp" line="41"/>
        <source>Knotter</source>
        <translation type="unfinished">Knotter</translation>
    </message>
</context>
<context>
    <name>Brush_Style</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="448"/>
        <source>Change Brush Style</source>
        <translation>Změnit styl štětce</translation>
    </message>
</context>
<context>
    <name>Change_Borders</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="228"/>
        <source>Change Border</source>
        <translation>Změnit ohraničení</translation>
    </message>
</context>
<context>
    <name>Change_Colors</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="191"/>
        <source>Change Color</source>
        <translation>Změnit barvu</translation>
    </message>
</context>
<context>
    <name>Change_Edge_Style</name>
    <message>
        <source>Change Edge Type</source>
        <translation type="obsolete">Změnit druh oblouku</translation>
    </message>
</context>
<context>
    <name>Change_Edge_Type</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="721"/>
        <source>Change Edge Type</source>
        <translation>Změnit druh oblouku</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="14"/>
        <source>Select Color</source>
        <translation type="unfinished">Vybrat barvu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="60"/>
        <source>Saturation</source>
        <translation type="unfinished">Sytost</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="67"/>
        <source>Hue</source>
        <translation type="unfinished">Barevný odstín</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="84"/>
        <source>Hex</source>
        <translation type="unfinished">Šestnáctkový (hexadecimální)</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="91"/>
        <source>Blue</source>
        <translation type="unfinished">Modrá</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="128"/>
        <source>Value</source>
        <translation type="unfinished">Hodnota</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="135"/>
        <source>Green</source>
        <translation type="unfinished">Zelená</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="142"/>
        <source>Alpha</source>
        <translation type="unfinished">Alfa</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="149"/>
        <source>Red</source>
        <translation type="unfinished">Červená</translation>
    </message>
</context>
<context>
    <name>Color_Dialog</name>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="318"/>
        <source>Select Color</source>
        <translation>Vybrat barvu</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="319"/>
        <source>Saturation</source>
        <translation>Sytost</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="320"/>
        <source>Hue</source>
        <translation>Barevný odstín</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="321"/>
        <source>Hex</source>
        <translation>Šestnáctkový (hexadecimální)</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="322"/>
        <source>Blue</source>
        <translation>Modrá</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="323"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="324"/>
        <source>Green</source>
        <translation>Zelená</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="325"/>
        <source>Alpha</source>
        <translation>Alfa</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="326"/>
        <source>Red</source>
        <translation>Červená</translation>
    </message>
    <message>
        <source>Pick</source>
        <translation type="vanished">Vybrat</translation>
    </message>
    <message>
        <source>HHHhhhhh</source>
        <translation type="obsolete">HHHhhhhh</translation>
    </message>
</context>
<context>
    <name>Context_Menu_Edge</name>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="36"/>
        <source>Snap to grid</source>
        <translation>Přichytávat k mřížce</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="37"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="39"/>
        <source>Edge type</source>
        <translation>Typ oblouku</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="43"/>
        <source>Break on intersections</source>
        <translation>Přerušit při protnutí</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="44"/>
        <source>Subdivide...</source>
        <translation>Rozdělit dále...</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="46"/>
        <source>Properties...</source>
        <translation>Vlastnosti...</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="47"/>
        <source>Reset custom style</source>
        <translation>Nastavit vlastní styl znovu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="51"/>
        <source>Edge Context Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="80"/>
        <source>Snap to Grid</source>
        <translation>Přichytávat k mřížce</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="176"/>
        <source>Break edge</source>
        <translation>Přerušit oblouk</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="177"/>
        <source>Number of segments</source>
        <translation>Počet částí</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="181"/>
        <source>Subdivide Edge</source>
        <translation>Rozdělit oblouk dále</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="225"/>
        <source>Reset Edge Style</source>
        <translation>Nastavit styl oblouku znovu</translation>
    </message>
</context>
<context>
    <name>Context_Menu_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="35"/>
        <source>Snap to grid</source>
        <translation>Přichytávat k mřížce</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="36"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="37"/>
        <source>Properties...</source>
        <translation>Vlastnosti...</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="38"/>
        <source>Reset custom style</source>
        <translation>Nastavit vlastní styl znovu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="40"/>
        <source>Node Context Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="56"/>
        <source>Reset Node Style</source>
        <translation>Nastavit styl uzlu znovu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="79"/>
        <source>Snap to Grid</source>
        <translation>Přichytávat k mřížce</translation>
    </message>
</context>
<context>
    <name>Create_Edge</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="113"/>
        <source>Create Edge</source>
        <translation>Vytvořit oblouk</translation>
    </message>
</context>
<context>
    <name>Create_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="82"/>
        <source>Create Node</source>
        <translation>Vytvořit uzel</translation>
    </message>
</context>
<context>
    <name>Crossing_Style_Widget</name>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="33"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="139"/>
        <source>Curve</source>
        <translation>Křivka</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="46"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="141"/>
        <source>Size of the curve control handles</source>
        <translation>Velikost úchopů pro ovládání křivky</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="49"/>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="78"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="143"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="148"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="65"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="144"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="75"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="146"/>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation>Vzdálenost konců vlákna jdoucího pod jiným vláknem</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="91"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="149"/>
        <source>Gap</source>
        <translation>Mezera</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="117"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="151"/>
        <source>Slide</source>
        <translation>Posun</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="127"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="153"/>
        <source>Position of the crossing in the edge</source>
        <translation>Poloha křížení v oblouku</translation>
    </message>
</context>
<context>
    <name>Cusp_Style_Dialog</name>
    <message>
        <location filename="../../src/dialogs/cusp_style_dialog.ui" line="14"/>
        <location filename="../../src/generated/ui_cusp_style_dialog.h" line="66"/>
        <source>Node Preferences</source>
        <translation>Nastavení uzlu</translation>
    </message>
</context>
<context>
    <name>Cusp_Style_Widget</name>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="55"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="152"/>
        <source>Curve</source>
        <translation>Křivka</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="103"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="161"/>
        <source>Size of the curve control handles</source>
        <translation>Velikost úchopů pro ovládání křivky</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="84"/>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="106"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="159"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="163"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <source>Gap</source>
        <translation type="obsolete">Mezera</translation>
    </message>
    <message>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation type="obsolete">Vzdálenost konců vlákna jdoucího pod jiným vláknem</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="39"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="151"/>
        <source>Angle</source>
        <translation>Úhel</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="20"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="148"/>
        <source>Minimum angle required to render a cusp</source>
        <translation>Nejmenší úhel požadovaný pro udělání špičky</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="26"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="150"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="119"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="164"/>
        <source>Shape</source>
        <translation>Tvar</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="145"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="165"/>
        <source>Distance</source>
        <translation>Vzdálenost</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="81"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="157"/>
        <source>Distance of the cusp tip from the node position</source>
        <translation>Vzdálenost rady ke špičce od polohy uzlu</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Styl</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="68"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="154"/>
        <source>Shape style</source>
        <translation>Styl tvaru</translation>
    </message>
</context>
<context>
    <name>Custom_Colors</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="265"/>
        <source>Custom Colors</source>
        <translation>Vlastní barvy</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="267"/>
        <source>Auto Color</source>
        <translation>Automatické barvy</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <source>px</source>
        <translation type="obsolete">px</translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="obsolete">Šířka</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Název</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
    <message>
        <source>°</source>
        <translation type="obsolete">°</translation>
    </message>
</context>
<context>
    <name>Dialog_Confirm_Close</name>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="103"/>
        <source>Close Files</source>
        <translation>Zavřít soubory</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="20"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="104"/>
        <source>The following files have unsaved changes:</source>
        <translation>Následující soubory mají neuložené změny:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="45"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="105"/>
        <source>Save selected files?</source>
        <translation>Uložit vybrané soubory?</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="54"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="106"/>
        <source>&amp;Save Selected</source>
        <translation>&amp;Uložit vybrané</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="64"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="107"/>
        <source>Do&amp;n&apos;t Save</source>
        <translation>&amp;Neukládat</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="74"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="108"/>
        <source>&amp;Cancel</source>
        <translation>Z&amp;rušit</translation>
    </message>
</context>
<context>
    <name>Dialog_Download_Plugin</name>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.ui" line="22"/>
        <location filename="../../src/generated/ui_dialog_download_plugin.h" line="92"/>
        <source>Progress:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.ui" line="54"/>
        <location filename="../../src/generated/ui_dialog_download_plugin.h" line="94"/>
        <source>File</source>
        <translation type="unfinished">Soubor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.ui" line="59"/>
        <location filename="../../src/generated/ui_dialog_download_plugin.h" line="96"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="37"/>
        <source>Installing %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="51"/>
        <source>Queued</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="106"/>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="157"/>
        <source>Download Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="107"/>
        <source>Error during the installation of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="113"/>
        <source>Download Complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="114"/>
        <source>%1 was installed successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="164"/>
        <source>Download Successful</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog_Edge_Properties</name>
    <message>
        <location filename="../../src/dialogs/dialog_edge_properties.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_edge_properties.h" line="60"/>
        <source>Edge Properties</source>
        <translation>Vlastnosti oblouku</translation>
    </message>
</context>
<context>
    <name>Dialog_Insert_Polygon</name>
    <message>
        <source>Insert Polygon</source>
        <translation type="obsolete">Vložit mnohoúhelník</translation>
    </message>
    <message>
        <source>Sides</source>
        <translation type="obsolete">Strany</translation>
    </message>
    <message>
        <source>Whether there should be a node connected to the vertices on the center of the polygon</source>
        <translation type="obsolete">Zda má být uzel spojený s vrcholy ve středu mnohoúhelníku</translation>
    </message>
    <message>
        <source>Node at Center</source>
        <translation type="obsolete">Uzel ve středu</translation>
    </message>
</context>
<context>
    <name>Dialog_Insert_Star</name>
    <message>
        <source>Sides</source>
        <translation type="obsolete">Strany</translation>
    </message>
    <message>
        <source>px</source>
        <translation type="obsolete">px</translation>
    </message>
</context>
<context>
    <name>Dialog_Plugins</name>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="486"/>
        <source>Plugins</source>
        <translation>Přídavné moduly</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Povolit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="34"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="500"/>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="132"/>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="213"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="487"/>
        <source>Disabled</source>
        <translation>Zakázáno</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="142"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="488"/>
        <source>Reload Script</source>
        <translation>Nahrát skript znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="154"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="489"/>
        <source>Edit Script</source>
        <translation>Upravit skript</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="175"/>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="385"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="490"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="503"/>
        <source>Plugin data</source>
        <translation>Data přídavného modulu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="192"/>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="411"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="492"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="505"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="197"/>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="416"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="494"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="507"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="207"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="495"/>
        <source>View &amp;Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="219"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="496"/>
        <source>C&amp;lear Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="239"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="497"/>
        <source>No plugin installed</source>
        <translation>Žádný přídavný modul není nainstalován</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="248"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="498"/>
        <source>&amp;Reload Plugins</source>
        <translation>&amp;Nahrát přídavné moduly znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="260"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="499"/>
        <source>&amp;Create...</source>
        <translation>&amp;Vytvořit...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="280"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="510"/>
        <source>Browse Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="316"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="501"/>
        <source>&amp;Refresh List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="366"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="502"/>
        <source>&amp;Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="424"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="508"/>
        <source>Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="467"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="509"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušit</translation>
    </message>
    <message>
        <source>&amp;Install...</source>
        <translation type="obsolete">&amp;Instalovat...</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <source>Reload Plugins</source>
        <translation type="obsolete">Nahrát přídavné moduly znovu</translation>
    </message>
    <message>
        <source>Install...</source>
        <translation type="obsolete">Instalovat...</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Zavřít</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="88"/>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="331"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="213"/>
        <source>Enabled</source>
        <translation>Povoleno</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="251"/>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="271"/>
        <source>Finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="276"/>
        <source>Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="350"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="372"/>
        <source>No plugin loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="386"/>
        <source>You don&apos;t have this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="395"/>
        <source>You already have this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="399"/>
        <source>You can upgrade this plugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog_Preferences</name>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="686"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="125"/>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="194"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="693"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="706"/>
        <source>Toolbars</source>
        <translation>Nástroje</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="112"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="691"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="138"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="695"/>
        <source>Performance</source>
        <translation>Výkon</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="148"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="697"/>
        <source>Interface</source>
        <translation>Rozhraní</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="393"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="730"/>
        <source>Icon Size</source>
        <translation>Velikost ikony</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="400"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="732"/>
        <source>Icon size</source>
        <translation>Velikost ikony</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="403"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="735"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the size used for toolbar icons and items in the drawing area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Vyberte velikost použitou pro ikony v nástrojovém pruhu a položky v oblasti, ve které se kreslí.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="410"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="737"/>
        <source>Toolbar Button Style</source>
        <translation>Styl tlačítka v nástrojovém panelu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="417"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="739"/>
        <source>Select how the toolbar should display icons and text</source>
        <translation>Vyberte, jak má nástrojový pruh zobrazovat ikony a text</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="424"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="741"/>
        <source>Toolbar Buttons</source>
        <translation>Tlačítka v nástrojovém panelu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="191"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="704"/>
        <source>Save the customized toolbar buttons</source>
        <translation>Uložit vlastní nastavení tlačítek v nástrojovém pruhu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="204"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="708"/>
        <source>Dialogs, window size and position</source>
        <translation>Nástrojové pruhy, dialogy, velikost okna a umístění</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="217"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="711"/>
        <source>Knot Style</source>
        <translation>Styl keltského uzlu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="266"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="719"/>
        <source>Max Recent Files</source>
        <translation>Největší počet naposledy otevřených souborů</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="256"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="718"/>
        <source>Check for unsaved files on close</source>
        <translation>Při zavření provést kontrolu zaměřenou na neuložené soubory</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="234"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="714"/>
        <source>Clear recent file history</source>
        <translation>Vyprázdnit historii nedávných souborů</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="237"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="716"/>
        <source>Clear</source>
        <translation>Vyprázdnit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="249"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="717"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="185"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="702"/>
        <source>Save between sessions</source>
        <translation>Uložit mezi sezeními</translation>
    </message>
    <message>
        <source>Toobars, dialogs, window size and position</source>
        <translation type="obsolete">Nástrojové pruhy, dialogy, velikost oken a umístění</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="207"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="710"/>
        <source>User Interface</source>
        <translation>Uživatelské rozhraní</translation>
    </message>
    <message>
        <source>Knot style settings</source>
        <translation type="obsolete">Nastavení stylu keltského uzlu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="224"/>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="681"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="712"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="783"/>
        <source>Grid</source>
        <translation>Mřížka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="374"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="727"/>
        <source>Clear configuration and restore defaults next time Knotter is launched</source>
        <translation>Smazat nastavení a obnovit výchozí nastavení, až bude Knotter příště spuštěn</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="377"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="729"/>
        <source>Clear  saved configuration</source>
        <translation>Smazat uložené nastavení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="289"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="720"/>
        <source>Clipboard</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="158"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="699"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="304"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="721"/>
        <source>Knot</source>
        <translation>Uzel</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="314"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="722"/>
        <source>XML</source>
        <translation>XML</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="329"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="723"/>
        <source>SVG</source>
        <translation>SVG</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="344"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="724"/>
        <source>PNG</source>
        <translation>PNG</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="359"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="725"/>
        <source>TIFF</source>
        <translation>TIFF</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="440"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="742"/>
        <source>Rendering</source>
        <translation>Kreslení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="446"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="744"/>
        <source>Enable caching of the rendered knot image</source>
        <translation>Povolit ukládání dělaného obrázku keltského uzlu do vyrovnávací paměti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="449"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="747"/>
        <source>Enable caching of the rendered knot image. Imporves performance in large scenes but may create some artifacts.</source>
        <translation>Povolit ukládání dělaného obrázku keltského uzlu do vyrovnávací paměti. Vylepší výkon u velkých scén, ale může vést i k něčemu nechtěnému.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="452"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="749"/>
        <source>Cache image</source>
        <translation>Ukládat obrázek do vyrovnávací paměti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="459"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="751"/>
        <source>Redraw the knot while moving nodes</source>
        <translation>Překreslit keltský uzel při přesunu uzlů</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="462"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="754"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When selected the knot will be updated fluidly when moving nodes.&lt;/p&gt;&lt;p&gt;When disabled, will update only after the nodes have been moved.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Je-li povoleno, keltský uzel se bude obnovovat plynule při přesunu uzlů.&lt;/p&gt;&lt;p&gt;Je-li zakázáno, keltský uzel se bude obnovovat až po přesunu uzlů.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="465"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="756"/>
        <source>Fluid refresh</source>
        <translation>Plynulá obnova</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="475"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="757"/>
        <source>Antialiasing</source>
        <translation>Vyhlazování</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="485"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="758"/>
        <source>Script</source>
        <translation>Skript</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="491"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="759"/>
        <source>Timeout</source>
        <translation>Časové omezení na provedení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="498"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="761"/>
        <source>Maximum execution time for scripts in seconds, 0 means no limit.</source>
        <translation>Nejvyšší hodnota času provedení skriptů v sekundách, 0 znamená bez omezení.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="501"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="763"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="531"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="764"/>
        <source>Node Icon Size</source>
        <translation>Velikost ikony uzlu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="538"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="765"/>
        <source>Widget Style</source>
        <translation>Styl rozhraní</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="548"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="767"/>
        <source>User interface style. Some widgets may require a restart to take effect.</source>
        <translation>Styl uživatelského rozhraní. Některé doplňky vyžadují k tomu, aby se projevily, opětovné spuštění programu.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="555"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="770"/>
        <source>Shows a preview of the widget style</source>
        <translation>Ukáže náhled stylu doplňku</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="558"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="772"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="564"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="773"/>
        <source>Button</source>
        <translation>Tlačítko</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="575"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="774"/>
        <source>Item1</source>
        <translation>Položka 1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="580"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="775"/>
        <source>Item2</source>
        <translation>Položka 2</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="585"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="776"/>
        <source>Item3</source>
        <translation>Položka 3</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="596"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="778"/>
        <source>Colors</source>
        <translation>Barvy</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="635"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="779"/>
        <source>Edge (Highlighted)</source>
        <translation>Oblouk (zvýrazněný)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="651"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="780"/>
        <source>Edge (Selected)</source>
        <translation>Oblouk (vybraný)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="664"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="781"/>
        <source>Edge (Resting)</source>
        <translation>Oblouk (normální)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="671"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="782"/>
        <source>Node (Resting)</source>
        <translation>Uzel (normální)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="691"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="784"/>
        <source>Node (Selected)</source>
        <translation>Uzel (vybraný)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="698"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="785"/>
        <source>Node (Highlighted)</source>
        <translation>Uzel (zvýrazněný)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="744"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="786"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="754"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="787"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="764"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="788"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="99"/>
        <source>Small (16x16)</source>
        <translation>Malá (16x16)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="100"/>
        <source>Medium (22x22)</source>
        <translation>Střední (22x22)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="101"/>
        <source>Large (48x48)</source>
        <translation>Velká (48x48)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="102"/>
        <source>Huge (64x64)</source>
        <translation>Obrovská (64x64)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="108"/>
        <source>Icon Only</source>
        <translation>Pouze ikona</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="109"/>
        <source>Text Only</source>
        <translation>Pouze text</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="110"/>
        <source>Text Beside Icon</source>
        <translation>Text vedle ikony</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="111"/>
        <source>Text Under Icon</source>
        <translation>Text pod ikonou</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="112"/>
        <source>Follow System Style</source>
        <translation>Následovat systémový styl</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="119"/>
        <source>Small</source>
        <translation>Malá</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="120"/>
        <source>Medium</source>
        <translation>Střední</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="121"/>
        <source>Large</source>
        <translation>Velká</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="191"/>
        <source>Clearing Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="192"/>
        <source>Next time %1 will start with the default settings.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Display_Border</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="287"/>
        <source>Display Borders</source>
        <translation>Zobrazit okraje</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="289"/>
        <source>Hide Borders</source>
        <translation>Skrýt okraje</translation>
    </message>
</context>
<context>
    <name>Dock_Background</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="17"/>
        <location filename="../../src/generated/ui_dock_background.h" line="152"/>
        <source>Background</source>
        <translation>Pozadí</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="42"/>
        <location filename="../../src/generated/ui_dock_background.h" line="153"/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="72"/>
        <location filename="../../src/generated/ui_dock_background.h" line="154"/>
        <source>Image</source>
        <translation>Obrázek</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="84"/>
        <location filename="../../src/generated/ui_dock_background.h" line="155"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="94"/>
        <location filename="../../src/generated/ui_dock_background.h" line="156"/>
        <source>Browse...</source>
        <translation>Procházet...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="106"/>
        <location filename="../../src/generated/ui_dock_background.h" line="157"/>
        <source>Scale</source>
        <translation>Změnit velikost</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="113"/>
        <location filename="../../src/generated/ui_dock_background.h" line="158"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="129"/>
        <location filename="../../src/generated/ui_dock_background.h" line="159"/>
        <source>Move...</source>
        <translation>Posunout...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.cpp" line="68"/>
        <source>Open background image</source>
        <translation>Otevřít obrázek s pozadím</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.cpp" line="70"/>
        <source>All supported images (%1)</source>
        <translation>Všechny podporované obrázky (%1)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.cpp" line="70"/>
        <source>All files (*)</source>
        <translation>Všechny soubory (*)</translation>
    </message>
</context>
<context>
    <name>Dock_Borders</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_borders.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_borders.h" line="69"/>
        <source>Border</source>
        <translation>Okraj</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_borders.ui" line="26"/>
        <location filename="../../src/generated/ui_dock_borders.h" line="70"/>
        <source>Enable Borders</source>
        <translation>Povolit okraje</translation>
    </message>
</context>
<context>
    <name>Dock_Grid</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="197"/>
        <source>Configure Grid</source>
        <translation>Nastavit mřížku</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="50"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="198"/>
        <source>Enable Grid</source>
        <translation>Povolit mřížku</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="65"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="199"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="72"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="200"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="119"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="205"/>
        <source>Shape</source>
        <translation>Tvar</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="86"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="201"/>
        <source>Square</source>
        <translation>Čtverec</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="96"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="202"/>
        <source>Triangle 1</source>
        <translation>Trojúhelník 1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="106"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="203"/>
        <source>Triangle 2</source>
        <translation>Trojúhelník 2</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="128"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="206"/>
        <source>Origin</source>
        <translation type="unfinished">Původ</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="183"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="212"/>
        <source>Reset</source>
        <translation>Nastavit znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="140"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="207"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="180"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="210"/>
        <source>Reset grid position</source>
        <translation>Nastavit polohu mřížky znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="190"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="213"/>
        <source>Move...</source>
        <translation>Posunout...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="163"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="208"/>
        <source>y</source>
        <translation>y</translation>
    </message>
</context>
<context>
    <name>Dock_Knot_Display</name>
    <message>
        <source>Knot Display</source>
        <translation type="obsolete">Zobrazení uzlu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="17"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="142"/>
        <source>Appearance</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="120"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="151"/>
        <source>Custom Colors</source>
        <translation>Vlastní barvy</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="79"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="147"/>
        <source>Width</source>
        <translation>Šířka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="86"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="148"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="113"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="150"/>
        <source>Joint Style</source>
        <translation>Styl spojení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="46"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="143"/>
        <source>Bevel</source>
        <translation>Úkos</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="56"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="144"/>
        <source>Miter</source>
        <translation>Pokos</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="66"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="145"/>
        <source>Round</source>
        <translation>Kolo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="106"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="149"/>
        <source>Pattern</source>
        <translation>Vzor</translation>
    </message>
</context>
<context>
    <name>Dock_Knot_Style</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="192"/>
        <source>Default Style</source>
        <translation type="unfinished">Výchozí styl</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="56"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="193"/>
        <source>Curve</source>
        <translation type="unfinished">Křivka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="66"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="195"/>
        <source>Size of the curve control handles</source>
        <translation type="unfinished">Velikost úchopů pro ovládání křivky</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="69"/>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="142"/>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="209"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="197"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="208"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="218"/>
        <source>px</source>
        <translation type="unfinished">px</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="82"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="198"/>
        <source>Nodes</source>
        <translation type="unfinished">Uzly</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="100"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="199"/>
        <source>Angle</source>
        <translation type="unfinished">Úhel</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="110"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="201"/>
        <source>Minimum angle required to render a cusp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="116"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="203"/>
        <source>°</source>
        <translation type="unfinished">°</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="129"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="204"/>
        <source>Distance</source>
        <translation type="unfinished">Vzdálenost</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="139"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="206"/>
        <source>Distance of the cusp tip from the node position</source>
        <translation type="unfinished">Vzdálenost rady ke špičce od polohy uzlu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="158"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="209"/>
        <source>Shape</source>
        <translation type="unfinished">Tvar</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="168"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="211"/>
        <source>Shape style</source>
        <translation type="unfinished">Styl tvaru</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="178"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="213"/>
        <source>Edges</source>
        <translation type="unfinished">Oblouky</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="196"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="214"/>
        <source>Gap</source>
        <translation type="unfinished">Mezera</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="206"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="216"/>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation type="unfinished">Vzdálenost konců vlákna jdoucího pod jiným vláknem</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="228"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="219"/>
        <source>Slide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="238"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="221"/>
        <source>Position of the crossing in the edge</source>
        <translation type="unfinished">Poloha křížení v oblouku</translation>
    </message>
</context>
<context>
    <name>Dock_Script_Log</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="364"/>
        <source>Script Console</source>
        <translation>Skriptovací konzole</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="72"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="365"/>
        <source>&amp;New</source>
        <translation>&amp;Nový</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="87"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="366"/>
        <source>&amp;Open</source>
        <translation>&amp;Otevřít</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="99"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="367"/>
        <source>&amp;Save</source>
        <translation>&amp;Uložit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="111"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="368"/>
        <source>Save &amp;As</source>
        <translation>Uložit &amp;jako</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="133"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="369"/>
        <source>&amp;Undo</source>
        <translation>&amp;Zpět</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="148"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="370"/>
        <source>R&amp;edo</source>
        <translation>Z&amp;novu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="167"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="372"/>
        <source>External Editor</source>
        <translation>Vnější editor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="170"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="374"/>
        <source>E&amp;xternal Editor</source>
        <translation>&amp;Vnější editor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="182"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="376"/>
        <source>If enabled, this dialog won&apos;t be dockable</source>
        <translation>Pokud povoleno, tento dialog nebude ukotvitelný</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="185"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="378"/>
        <source>S&amp;tand Alone Dialog</source>
        <translation>S&amp;amostatný dialog</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="207"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="380"/>
        <source>Deploy Plugin</source>
        <translation>Nasadit přídavný modul</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="210"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="382"/>
        <source>&amp;Deploy Plugin</source>
        <translation>&amp;Nasadit přídavný modul</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="252"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="383"/>
        <source>Run</source>
        <translation>Spustit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="290"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="384"/>
        <source>&amp;Run</source>
        <translation>&amp;Spustit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="302"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="385"/>
        <source>Clear Output</source>
        <translation>Smazat výstup</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="317"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="387"/>
        <source>Stop the currently running script</source>
        <translation>Zastavit nyní běžící skript</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="320"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="389"/>
        <source>Abort Scripts</source>
        <translation>Přerušit skripty</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="140"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="209"/>
        <source>Script Editor</source>
        <translation>Editor skriptu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="249"/>
        <source>Open Script</source>
        <translation>Otevřít skript</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="270"/>
        <source>Save Script</source>
        <translation>Uložit skript</translation>
    </message>
</context>
<context>
    <name>Edge_Style_All</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="860"/>
        <source>Change Edge Style</source>
        <translation>Změnit styl oblouku</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Crossing_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="679"/>
        <source>Change Selection Crossing Gap</source>
        <translation>Změnit mezeru křížení ve výběru</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Edge_Slide</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="698"/>
        <source>Slide Selected Edges</source>
        <translation>Posunout vybrané oblouky</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Enable</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="815"/>
        <source>Toggle Selection Style Property</source>
        <translation>Přepnout vlastnost styl výběru</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Handle_Lenght</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="759"/>
        <source>Change Edge Curve Control</source>
        <translation>Změnit ovládání křivky oblouku</translation>
    </message>
</context>
<context>
    <name>Export_Image_Dialog</name>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="14"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="247"/>
        <source>Export Image</source>
        <translation>Vyvést obrázek</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="25"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="248"/>
        <source>Include Graph in output</source>
        <translation>Zahrnout graf do výstupu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="32"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="249"/>
        <source>Include background image</source>
        <translation>Zahrnout obrázek pozadí</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="45"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="250"/>
        <source>Vector</source>
        <translation>Vektor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="51"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="251"/>
        <source>Export &amp;SVG...</source>
        <translation>Vyvést jako &amp;SVG...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="72"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="252"/>
        <source>Raster</source>
        <translation>Rastr</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="78"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="254"/>
        <source>Avoid jagged edges</source>
        <translation>Vyvarovat se zubatých okrajů</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="81"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="256"/>
        <source>Antialiasing</source>
        <translation>Vyhlazování okrajů</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="97"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="257"/>
        <source>Compression</source>
        <translation>Komprese</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="106"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="259"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;On some formats, higher compression means less quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;U některých formátů vyšší komprese znamená menší jakost.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation type="obsolete">Jakost</translation>
    </message>
    <message>
        <source>Depending on the format, lower quality means more compressed files.</source>
        <translation type="obsolete">V závislosti na formátu znamená menší jakost více komprimované soubory.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="137"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="261"/>
        <source>Width</source>
        <translation>Šířka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="144"/>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="164"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="262"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="264"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="157"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="263"/>
        <source>Height</source>
        <translation>Výška</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="177"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="265"/>
        <source>R&amp;eset Size</source>
        <translation>Nastavit v&amp;elikost znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="184"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="266"/>
        <source>Export &amp;Image...</source>
        <translation>Vyvést &amp;obrázek...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="202"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="268"/>
        <source>Keep Ratio</source>
        <translation>Zachovat poměr stran</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="205"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="270"/>
        <source>Keep &amp;Ratio</source>
        <translation>Zachovat &amp;poměr stran</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="223"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="271"/>
        <source>Background</source>
        <translation>Pozadí</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="80"/>
        <source>%1%</source>
        <extracomment>Compression percentage</extracomment>
        <translation>%1%</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="86"/>
        <source>Export Knot as SVG</source>
        <translation>Vyvést keltský uzel jako SVG</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="111"/>
        <source>File Error</source>
        <translation>Chyba souboru</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="111"/>
        <source>Could not write to &quot;%1&quot;.</source>
        <translation>Nepodařilo se zapsat do &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="125"/>
        <source>PNG Images (*.png)</source>
        <translation>Obrázky PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="126"/>
        <source>Jpeg Images (*.jpg *.jpeg)</source>
        <translation>Obrázky JPEG (*.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="127"/>
        <source>Bitmap (*.bmp)</source>
        <translation>Bitmapa (*.bmp)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="128"/>
        <source>All supported images (%1)</source>
        <translation>Všechny podporované obrázky (%1)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="129"/>
        <source>All files (*)</source>
        <translation>Všechny soubory (*)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="133"/>
        <source>Export Knot as Image</source>
        <translation>Vyvést keltský uzel jako obrázek</translation>
    </message>
</context>
<context>
    <name>KeySequence_ListWidget</name>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="66"/>
        <source>Clear</source>
        <translation type="unfinished">Vyprázdnit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="67"/>
        <source>Clear shortcut for &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="73"/>
        <source>Reset</source>
        <translation type="unfinished">Nastavit znovu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="74"/>
        <source>Reset shortcut for &quot;%1&quot; to the default %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeySequence_Widget</name>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_widget.cpp" line="88"/>
        <source>(empty)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Knot_Style_All</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="678"/>
        <source>Set Knot Style</source>
        <translation>Nastavit styl uzlu</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Crossing_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="387"/>
        <source>Change Crossing Gap</source>
        <translation>Změnit mezeru křížení</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Cusp_Angle</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="421"/>
        <source>Change Minimum Cusp Angle</source>
        <translation>Změnit nejmenší úhel pro špičku</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Cusp_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="439"/>
        <source>Change Cusp Distance</source>
        <translation>Změnit vzdálenost špičky</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Cusp_Shape</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="544"/>
        <source>Change Cusp Shape</source>
        <translation>Změnit tvar špičky</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Edge_Slide</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="404"/>
        <source>Slide Crossing</source>
        <translation>Posunout křížení</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Handle_Lenght</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="370"/>
        <source>Change Curve Control</source>
        <translation>Změnit ovládání křivky</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Widget</name>
    <message>
        <source>Curve</source>
        <translation type="obsolete">Křivka</translation>
    </message>
    <message>
        <source>Size of the curve control handles</source>
        <translation type="obsolete">Velikost úchopů pro ovládání křivky</translation>
    </message>
    <message>
        <source>px</source>
        <translation type="obsolete">px</translation>
    </message>
    <message>
        <source>Nodes</source>
        <translation type="obsolete">Uzly</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="obsolete">Úhel</translation>
    </message>
    <message>
        <source>Minimum angle required to render a cusp</source>
        <translation type="obsolete">Nejmenší úhel požadovaný pro nakreslení špičky</translation>
    </message>
    <message>
        <source>°</source>
        <translation type="obsolete">°</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="obsolete">Vzdálenost</translation>
    </message>
    <message>
        <source>Distance of the cusp tip from the node position</source>
        <translation type="obsolete">Vzdálenost rady ke špičce od polohy uzlu</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation type="obsolete">Tvar</translation>
    </message>
    <message>
        <source>Shape style</source>
        <translation type="obsolete">Styl tvaru</translation>
    </message>
    <message>
        <source>Edges</source>
        <translation type="obsolete">Oblouky</translation>
    </message>
    <message>
        <source>Gap</source>
        <translation type="obsolete">Mezera</translation>
    </message>
    <message>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation type="obsolete">Vzdálenost konců vlákna jdoucího pod jiným vláknem</translation>
    </message>
    <message>
        <source>Slide</source>
        <translation type="obsolete">Posunout</translation>
    </message>
    <message>
        <source>Position of the crossing in the edge</source>
        <translation type="obsolete">Poloha křížení v oblouku</translation>
    </message>
</context>
<context>
    <name>Knot_View</name>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="176"/>
        <source>Load File</source>
        <translation>Nahrát soubor</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="319"/>
        <source>Change Edge Type</source>
        <translation>Změnit druh oblouku</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="389"/>
        <source>Horizontal Flip</source>
        <translation>Vodorovné převrácení</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="404"/>
        <source>Vertical Flip</source>
        <translation>Svislé převrácení</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="466"/>
        <source>Break Edge</source>
        <translation>Přerušit oblouk</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="490"/>
        <source>Remove Node</source>
        <translation>Odstranit uzel</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="168"/>
        <source>Move Ahead</source>
        <translation>Posunout dopředu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="170"/>
        <source>Start Chain</source>
        <translation>Začít řetěz</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="172"/>
        <source>Create Node</source>
        <translation>Vytvořit uzel</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="174"/>
        <source>Add Edge</source>
        <translation>Přidat okraj</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="1010"/>
        <source>Move Nodes</source>
        <translation>Přesunout uzly</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="1024"/>
        <source>Rotate Nodes</source>
        <translation>Otočit uzly</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="1024"/>
        <source>Scale Nodes</source>
        <translation>Změnit velikost uzlů</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="131"/>
        <source>Snap to Grid</source>
        <translation>Přichytávat k mřížce</translation>
    </message>
</context>
<context>
    <name>Knot_Width</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="333"/>
        <source>Change Stroke Width</source>
        <translation>Změnit šířku čáry</translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="50"/>
        <location filename="../../src/generated/ui_main_window.h" line="827"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="54"/>
        <location filename="../../src/generated/ui_main_window.h" line="828"/>
        <source>Open &amp;Recent</source>
        <translation>Otevřít &amp;nedávný</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="82"/>
        <location filename="../../src/generated/ui_main_window.h" line="829"/>
        <source>&amp;Edit</source>
        <translation>Úp&amp;ravy</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="98"/>
        <location filename="../../src/generated/ui_main_window.h" line="830"/>
        <source>&amp;View</source>
        <translation>&amp;Pohled</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="102"/>
        <location filename="../../src/generated/ui_main_window.h" line="831"/>
        <source>&amp;Toolbars</source>
        <translation>&amp;Nástroje</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="122"/>
        <location filename="../../src/generated/ui_main_window.h" line="833"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Zvětšení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="145"/>
        <location filename="../../src/generated/ui_main_window.h" line="834"/>
        <source>&amp;Nodes</source>
        <translation>&amp;Uzly</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="162"/>
        <location filename="../../src/generated/ui_main_window.h" line="835"/>
        <source>&amp;Tools</source>
        <translation>&amp;Nástroje</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="172"/>
        <location filename="../../src/generated/ui_main_window.h" line="836"/>
        <source>&amp;Help</source>
        <translation>Nápo&amp;věda</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="182"/>
        <location filename="../../src/generated/ui_main_window.h" line="837"/>
        <source>&amp;Plugins</source>
        <translation>Mo&amp;duly</translation>
    </message>
    <message>
        <source>Insert objects from plugin</source>
        <translation type="obsolete">Vložit předměty z přídavného modulu</translation>
    </message>
    <message>
        <source>&amp;Insert</source>
        <translation type="obsolete">&amp;Vložka</translation>
    </message>
    <message>
        <source>Transform selection with plugin</source>
        <translation type="obsolete">Proměnit výběr přídavným modulem</translation>
    </message>
    <message>
        <source>&amp;Transform</source>
        <translation type="obsolete">&amp;Proměna</translation>
    </message>
    <message>
        <source>Main Toolbar</source>
        <translation type="obsolete">Hlavní pruh s nástroji</translation>
    </message>
    <message>
        <source>View Toolbar</source>
        <translation type="obsolete">Nástrojový pruh pro pohled</translation>
    </message>
    <message>
        <source>Drawing Toolbar</source>
        <translation type="obsolete">Nástrojový pruh pro kreslení</translation>
    </message>
    <message>
        <source>Transformation Toolbar</source>
        <translation type="obsolete">Nástrojový pruh pro proměnu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="287"/>
        <location filename="../../src/generated/ui_main_window.h" line="750"/>
        <source>&amp;Open...</source>
        <translation>&amp;Otevřít...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="297"/>
        <location filename="../../src/generated/ui_main_window.h" line="751"/>
        <source>&amp;New</source>
        <translation>&amp;Nový</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="307"/>
        <location filename="../../src/generated/ui_main_window.h" line="752"/>
        <source>&amp;Save</source>
        <translation>&amp;Uložit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="317"/>
        <location filename="../../src/generated/ui_main_window.h" line="753"/>
        <source>Save &amp;As...</source>
        <translation>Uložit &amp;jako...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="327"/>
        <location filename="../../src/generated/ui_main_window.h" line="754"/>
        <source>Save A&amp;ll</source>
        <translation>Uložit &amp;vše</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="330"/>
        <location filename="../../src/generated/ui_main_window.h" line="756"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="340"/>
        <location filename="../../src/generated/ui_main_window.h" line="758"/>
        <source>&amp;Print...</source>
        <translation>&amp;Tisk...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="350"/>
        <location filename="../../src/generated/ui_main_window.h" line="759"/>
        <source>Pa&amp;ge Setup...</source>
        <translation>Nastavení &amp;strany...</translation>
    </message>
    <message>
        <source>Print Pre&amp;view</source>
        <translation type="obsolete">Náhled &amp;tisku...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="370"/>
        <location filename="../../src/generated/ui_main_window.h" line="761"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="375"/>
        <location filename="../../src/generated/ui_main_window.h" line="762"/>
        <source>Close All</source>
        <translation>Zavřít vše</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="378"/>
        <location filename="../../src/generated/ui_main_window.h" line="764"/>
        <source>Ctrl+Shift+W</source>
        <translation>Ctrl+Shift+W</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="388"/>
        <location filename="../../src/generated/ui_main_window.h" line="766"/>
        <source>&amp;Exit</source>
        <translation>&amp;Ukončit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="398"/>
        <location filename="../../src/generated/ui_main_window.h" line="767"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopírovat</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="408"/>
        <location filename="../../src/generated/ui_main_window.h" line="768"/>
        <source>&amp;Paste</source>
        <translation>&amp;Vložit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="418"/>
        <location filename="../../src/generated/ui_main_window.h" line="769"/>
        <source>Cu&amp;t</source>
        <translation>Vyj&amp;mout</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="428"/>
        <location filename="../../src/generated/ui_main_window.h" line="770"/>
        <source>Select &amp;All</source>
        <translation>Vybrat &amp;vše</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="438"/>
        <location filename="../../src/generated/ui_main_window.h" line="771"/>
        <source>&amp;Preferences...</source>
        <translation>&amp;Nastavení...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="448"/>
        <location filename="../../src/generated/ui_main_window.h" line="772"/>
        <source>&amp;Reset View</source>
        <translation>Nastavit po&amp;hled znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="458"/>
        <location filename="../../src/generated/ui_main_window.h" line="773"/>
        <source>Zoom &amp;In</source>
        <translation>&amp;Přiblížit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="468"/>
        <location filename="../../src/generated/ui_main_window.h" line="774"/>
        <source>Zoom &amp;Out</source>
        <translation>&amp;Oddálit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="478"/>
        <location filename="../../src/generated/ui_main_window.h" line="775"/>
        <source>&amp;Reset Zoom</source>
        <translation>Nastavit &amp;zvětšení znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="481"/>
        <location filename="../../src/generated/ui_main_window.h" line="777"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="497"/>
        <location filename="../../src/generated/ui_main_window.h" line="779"/>
        <source>Display &amp;Graph</source>
        <translation>Zobrazit &amp;graf</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="611"/>
        <location filename="../../src/generated/ui_main_window.h" line="797"/>
        <source>&amp;Select</source>
        <translation>&amp;Vybrat</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="627"/>
        <location filename="../../src/generated/ui_main_window.h" line="801"/>
        <source>Edge &amp;Chain</source>
        <translation>Ř&amp;etězec oblouku</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="723"/>
        <location filename="../../src/generated/ui_main_window.h" line="816"/>
        <source>Report &amp;Bugs...</source>
        <translation>Nahlásit &amp;chyby...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="770"/>
        <location filename="../../src/generated/ui_main_window.h" line="823"/>
        <source>&amp;Fit View</source>
        <translation>&amp;Přizpůsobit zobrazení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="773"/>
        <location filename="../../src/generated/ui_main_window.h" line="825"/>
        <source>Scroll and zoom the view to display the entire knot</source>
        <translation>Pohybovat a přiblížit pohled tak, aby byl zobrazen celý uzel</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="112"/>
        <location filename="../../src/generated/ui_main_window.h" line="832"/>
        <source>&amp;Dialogs</source>
        <translation>&amp;Dialogy</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="189"/>
        <location filename="../../src/generated/ui_main_window.h" line="838"/>
        <source>&amp;Grid</source>
        <translation>&amp;Mřížka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="360"/>
        <location filename="../../src/generated/ui_main_window.h" line="760"/>
        <source>Print Pre&amp;view...</source>
        <translation>Ná&amp;hled tisku...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="507"/>
        <location filename="../../src/generated/ui_main_window.h" line="780"/>
        <source>&amp;Connect</source>
        <translation>&amp;Připojit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="510"/>
        <location filename="../../src/generated/ui_main_window.h" line="782"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="520"/>
        <location filename="../../src/generated/ui_main_window.h" line="784"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Odpojit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="530"/>
        <location filename="../../src/generated/ui_main_window.h" line="785"/>
        <source>&amp;Horizontal Flip</source>
        <translation>Vo&amp;dorovné převrácení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="540"/>
        <location filename="../../src/generated/ui_main_window.h" line="786"/>
        <source>&amp;Vertical Flip</source>
        <translation>Sv&amp;islé převrácení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="553"/>
        <location filename="../../src/generated/ui_main_window.h" line="787"/>
        <source>&amp;Rotate</source>
        <translation>O&amp;točit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="569"/>
        <location filename="../../src/generated/ui_main_window.h" line="788"/>
        <source>&amp;Scale</source>
        <translation>&amp;Změnit velikost</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="579"/>
        <location filename="../../src/generated/ui_main_window.h" line="789"/>
        <source>&amp;Merge</source>
        <translation>&amp;Sloučit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="582"/>
        <location filename="../../src/generated/ui_main_window.h" line="791"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="592"/>
        <location filename="../../src/generated/ui_main_window.h" line="793"/>
        <source>&amp;Erase</source>
        <translation>&amp;Vymazat</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="595"/>
        <location filename="../../src/generated/ui_main_window.h" line="795"/>
        <source>Del</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>&amp;Edit Graph</source>
        <translation type="obsolete">&amp;Upravit graf</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="614"/>
        <location filename="../../src/generated/ui_main_window.h" line="799"/>
        <source>Alt+Shift+E</source>
        <translation>Alt+Shift+E</translation>
    </message>
    <message>
        <source>Edge &amp;Loop</source>
        <translation type="obsolete">Olemovat &amp;smyčku</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="630"/>
        <location filename="../../src/generated/ui_main_window.h" line="803"/>
        <source>Alt+Shift+L</source>
        <translation>Alt+Shift+L</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="760"/>
        <location filename="../../src/generated/ui_main_window.h" line="822"/>
        <source>&amp;Toggle Edges</source>
        <translation>&amp;Přepnout okraje</translation>
    </message>
    <message>
        <source>Insert &amp;Polygon...</source>
        <translation type="obsolete">Vložit mnohoú&amp;helník...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="640"/>
        <location filename="../../src/generated/ui_main_window.h" line="805"/>
        <source>&amp;Refresh Path</source>
        <translation>&amp;Obnovit cestu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="650"/>
        <location filename="../../src/generated/ui_main_window.h" line="806"/>
        <source>&amp;Manual</source>
        <translation>Přír&amp;učka</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="660"/>
        <location filename="../../src/generated/ui_main_window.h" line="807"/>
        <source>&amp;About Knotter...</source>
        <translation>&amp;O programu Knotter...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="728"/>
        <location filename="../../src/generated/ui_main_window.h" line="817"/>
        <source>Select Connected</source>
        <translation>Vybrat spojený</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="731"/>
        <location filename="../../src/generated/ui_main_window.h" line="819"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="747"/>
        <location filename="../../src/generated/ui_main_window.h" line="821"/>
        <source>&amp;Enable Grid</source>
        <translation>Povo&amp;lit mřížku</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="670"/>
        <location filename="../../src/generated/ui_main_window.h" line="808"/>
        <source>E&amp;xport...</source>
        <translation>&amp;Vyvést...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="673"/>
        <location filename="../../src/generated/ui_main_window.h" line="810"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="683"/>
        <location filename="../../src/generated/ui_main_window.h" line="812"/>
        <source>Snap to &amp;Grid</source>
        <translation>&amp;Přichytávat k mřížce</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="693"/>
        <location filename="../../src/generated/ui_main_window.h" line="813"/>
        <source>&amp;Undo</source>
        <translation>&amp;Zpět</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="703"/>
        <location filename="../../src/generated/ui_main_window.h" line="814"/>
        <source>&amp;Redo</source>
        <translation>&amp;Znovu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="713"/>
        <location filename="../../src/generated/ui_main_window.h" line="815"/>
        <source>&amp;Configure Plugins...</source>
        <translation>&amp;Nastavit přídavné moduly...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="182"/>
        <source>Zoom</source>
        <translation>Zvětšení</translation>
    </message>
    <message>
        <source>Knot Style</source>
        <translation type="obsolete">Styl keltského uzlu</translation>
    </message>
    <message>
        <source>Selection Style</source>
        <translation type="obsolete">Styl výběru</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="281"/>
        <source>Action History</source>
        <translation>Historie činností</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="285"/>
        <source>Selected Nodes</source>
        <translation>Vybrané uzly</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="288"/>
        <source>Selected Edges</source>
        <translation>Vybrané oblouky</translation>
    </message>
    <message>
        <source>Default Style</source>
        <translation type="obsolete">Výchozí styl</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="162"/>
        <source>Viewport X position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="162"/>
        <source>Viewport Y position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="163"/>
        <source>Viewport width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="163"/>
        <source>Viewport height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="302"/>
        <source>Warning:</source>
        <translation>Varování:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="303"/>
        <source>Discarding old configuration</source>
        <translation>Zahodit staré nastavení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="309"/>
        <source>Load old configuration</source>
        <translation>Nahrát staré nastavení</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="310"/>
        <source>Knotter has detected configuration for version %1,
this is version %2.
Do you want to load it anyways?</source>
        <translation>Knotter zjistil nastavení pro verzi %1,
toto je verze %2.
Přesto je chcete nahrát?</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="538"/>
        <location filename="../../src/dialogs/main_window.cpp" line="674"/>
        <source>New Knot</source>
        <translation>Nový keltský uzel</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="545"/>
        <source>%1 - %2%3</source>
        <extracomment>Main window title * %1 is the program name * %2 is the file name * %3 is a star * or an empty string depending on whether the file was modified</extracomment>
        <translation>%1 - %2%3</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="682"/>
        <location filename="../../src/dialogs/main_window.cpp" line="847"/>
        <source>File Error</source>
        <translation>Chyba souboru</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="683"/>
        <source>Error while reading &quot;%1&quot;.</source>
        <translation>Chyba při čtení &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="718"/>
        <source>Close File</source>
        <translation>Zavřít soubor</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="719"/>
        <source>The file &quot;%1&quot; has been modified.
Do you want to save changes?</source>
        <translation>Soubor &quot;%1&quot; byl změněn.
Chcete uložit změny?</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="750"/>
        <source>Undo %1</source>
        <translation>Zpět %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="755"/>
        <source>Redo %1</source>
        <translation>Znovu %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="797"/>
        <source>Open Knot</source>
        <translation>Otevřít keltský uzel</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="827"/>
        <source>Knot files (*.knot);;XML files (*.xml);;All files (*)</source>
        <translation>Soubory Knot (*.knot);;Soubory XML (*.xml);;Všechny soubory (*)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="828"/>
        <source>Save Knot</source>
        <translation>Uložit keltský uzel</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="848"/>
        <source>Failed to save file &quot;%1&quot;.</source>
        <translation>Nepodařilo se uložit soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="855"/>
        <source>(%1,%2)</source>
        <extracomment>Displaying mouse position, %1 = x, %2 = y</extracomment>
        <translation>(%1,%2)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="876"/>
        <source>No recent files</source>
        <translation>Žádné nedávné soubory</translation>
    </message>
    <message>
        <source>Insert Polygon</source>
        <extracomment>Name of the undo command triggered when inserting a polygon</extracomment>
        <translation type="obsolete">Vložit mnohoúhelník</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1031"/>
        <source>Paste</source>
        <translation>Vložit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1043"/>
        <source>Cut</source>
        <translation>Vyjmout</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1094"/>
        <source>Drop</source>
        <translation>Upustit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1113"/>
        <source>Snap to Grid</source>
        <translation>Přichytávat k mřížce</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1128"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1193"/>
        <source>Connect Nodes</source>
        <translation>Spojit uzly</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1212"/>
        <source>Disconnect Nodes</source>
        <translation>Odpojit uzly</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1230"/>
        <source>Merge Nodes</source>
        <translation>Sloučit uzly</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="21"/>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="28"/>
        <source>Clear</source>
        <translation type="unfinished">Vyprázdnit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="58"/>
        <source>&amp;Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="67"/>
        <source>&amp;Action1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="70"/>
        <source>Ctrl+1</source>
        <translation type="unfinished">Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="75"/>
        <source>A&amp;ction2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="78"/>
        <source>Alt+2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Move_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="312"/>
        <source>Move Node</source>
        <translation>Přesunout uzel</translation>
    </message>
</context>
<context>
    <name>Node_Style_All</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="700"/>
        <source>Change Node Style</source>
        <translation>Změnit styl uzlu</translation>
    </message>
</context>
<context>
    <name>Node_Style_Base</name>
    <message>
        <source>Change Selection Style</source>
        <translation type="obsolete">Změnit styl výběru</translation>
    </message>
</context>
<context>
    <name>Node_Style_Crossing_Distance</name>
    <message>
        <source>Change Selection Crossing Gap</source>
        <translation type="obsolete">Změnit mezeru křížení ve výběru</translation>
    </message>
</context>
<context>
    <name>Node_Style_Cusp_Angle</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="547"/>
        <source>Change Selection Cusp Angle</source>
        <translation>Změnit úhel pro špičku ve výběru</translation>
    </message>
</context>
<context>
    <name>Node_Style_Cusp_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="530"/>
        <source>Change Selection Cusp Distance</source>
        <translation>Změnit vzdálenost špičky ve výběru</translation>
    </message>
</context>
<context>
    <name>Node_Style_Cusp_Shape</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="620"/>
        <source>Change Selection Cusp Shape</source>
        <translation>Změnit tvar špičky ve výběru</translation>
    </message>
</context>
<context>
    <name>Node_Style_Enable</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="642"/>
        <source>Toggle Selection Style Property</source>
        <translation>Přepnout vlastnost styl výběru</translation>
    </message>
</context>
<context>
    <name>Node_Style_Handle_Lenght</name>
    <message>
        <source>Change Selection Curve Control</source>
        <translation type="obsolete">Změnit ovládání křivky ve výběru</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="513"/>
        <source>Change Node Curve Control</source>
        <translation>Změnit ovládání křivky uzlu</translation>
    </message>
</context>
<context>
    <name>Pen_Join_Style</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="410"/>
        <source>Change Joint Style</source>
        <translation>Změnit styl spojení</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/command_line.cpp" line="83"/>
        <location filename="../../src/command_line.cpp" line="86"/>
        <location filename="../../src/string_toolbar.cpp" line="62"/>
        <source>Warning:</source>
        <translation>Varování:</translation>
    </message>
    <message>
        <location filename="../../src/command_line.cpp" line="84"/>
        <source>Missing file for argument %1</source>
        <translation>Chybí soubor pro argument %1</translation>
    </message>
    <message>
        <location filename="../../src/command_line.cpp" line="87"/>
        <source>No input file specified for option %1</source>
        <translation>Nezadán žádný vstupní soubor pro volbu %1</translation>
    </message>
    <message>
        <location filename="../../src/string_toolbar.cpp" line="63"/>
        <source>Unknown action %1</source>
        <translation>Neznámá činnost %1</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="111"/>
        <source>Regular</source>
        <translation>Normální</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="128"/>
        <source>Inverted</source>
        <translation>Obrácený</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="212"/>
        <source>Wall</source>
        <translation>Zeď</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="266"/>
        <source>Hole</source>
        <translation>Otvor</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="90"/>
        <source>Round</source>
        <translation>Zaoblený</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="104"/>
        <source>Pointed</source>
        <translation>Špičatý</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="118"/>
        <source>Ogee</source>
        <translation>Žlábkovnicový</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="132"/>
        <source>Polygonal</source>
        <translation>Mnohoúhelníkový</translation>
    </message>
    <message>
        <source>Move Nodes</source>
        <translation type="obsolete">Přesunout uzly</translation>
    </message>
    <message>
        <source>Plugin name not specified</source>
        <translation type="obsolete">Nezadán název přídavného modulu</translation>
    </message>
    <message>
        <source>Unknown plugin type </source>
        <translation type="obsolete">Neznámý typ přídavného modulu </translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="291"/>
        <source>%1:%2:Error: %3</source>
        <translation>%1:%2: Chyba: %3</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="144"/>
        <source>Cusp</source>
        <translation type="unfinished">Špička</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="145"/>
        <source>Crossing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="146"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="147"/>
        <source>Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="165"/>
        <source>Unknown plugin type</source>
        <translation>Neznámý druh přídavného modulu</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="172"/>
        <source>Missing script file</source>
        <translation>Chybí soubor se skriptem</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="178"/>
        <source>Error while opening script file %1</source>
        <translation>Chyba při otevírání souboru se skriptem %1</translation>
    </message>
    <message>
        <location filename="../../src/scripting/misc_script_functions.cpp" line="54"/>
        <source>Expected file name for run_script()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/scripting/misc_script_functions.cpp" line="57"/>
        <source>Cannot open file &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Remove_Edge</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="159"/>
        <source>Remove Edge</source>
        <translation>Odstranit oblouk</translation>
    </message>
</context>
<context>
    <name>Remove_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="383"/>
        <source>Remove Node</source>
        <translation>Odstranit uzel</translation>
    </message>
</context>
<context>
    <name>Resource_Manager</name>
    <message>
        <source>Knotter</source>
        <translation type="vanished">Knotter</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="82"/>
        <location filename="../../src/resource_manager.cpp" line="150"/>
        <location filename="../../src/resource_manager.cpp" line="201"/>
        <source>Warning:</source>
        <translation>Varování:</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="83"/>
        <source>Unrecognised translation file name pattern: %1</source>
        <translation>Nerozpoznaný vzor názvu souboru s překladem: %1</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="155"/>
        <source>Error on loading translation file %1 for language %2 (%3)</source>
        <translation>Chyba při nahrávání překladového souboru %1 pro jazyk %2 (%3)</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="202"/>
        <source>There is no translation for language %1 (%2)</source>
        <translation>Pro jazyk %1 (%2) není žádný překlad</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Neznámá chyba</translation>
    </message>
    <message>
        <source>%1: Error: %2</source>
        <translation type="vanished">%1: Chyba: %2</translation>
    </message>
    <message>
        <source>Script aborted</source>
        <translation type="vanished">Vynuceno ukončení skriptu</translation>
    </message>
    <message>
        <source>Script timeout</source>
        <translation type="obsolete">Překročení času skriptu</translation>
    </message>
</context>
<context>
    <name>Resource_Script</name>
    <message>
        <location filename="../../src/resource_script.cpp" line="257"/>
        <source>Cannot open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="322"/>
        <source>Unknown error</source>
        <translation type="unfinished">Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="325"/>
        <source>%1: Error: %2</source>
        <translation type="unfinished">%1: Chyba: %2</translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="359"/>
        <source>Script aborted</source>
        <translation type="unfinished">Vynuceno ukončení skriptu</translation>
    </message>
</context>
<context>
    <name>Script_Document</name>
    <message>
        <location filename="../../src/scripting/wrappers/script_document.cpp" line="77"/>
        <source>Script Insert</source>
        <translation>Vložení skriptu</translation>
    </message>
    <message>
        <location filename="../../src/scripting/wrappers/script_document.cpp" line="160"/>
        <source>Load File</source>
        <translation>Nahrát soubor</translation>
    </message>
</context>
<context>
    <name>Script_QTableWidget</name>
    <message>
        <location filename="../../src/scripting/wrappers/script_qtablewidget.cpp" line="88"/>
        <source>Argument is not a QTableWidget object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Script_Window_Dialog</name>
    <message>
        <location filename="../../src/scripting/wrappers/script_window.cpp" line="212"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../../src/settings.cpp" line="114"/>
        <source>Warning:</source>
        <translation>Varování:</translation>
    </message>
    <message>
        <location filename="../../src/settings.cpp" line="115"/>
        <source>Not loading toolbar without name</source>
        <translation>Nástrojový panel bez názvu se nenahrává</translation>
    </message>
</context>
<context>
    <name>Test_Window</name>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="14"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="172"/>
        <source>Test_Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="24"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="180"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="41"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="181"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Soubor</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="50"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="182"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="54"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="183"/>
        <source>Something</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="79"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="184"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="98"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="173"/>
        <source>New</source>
        <translation type="unfinished">Nový</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="108"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="174"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="118"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="175"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="128"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="176"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="138"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="177"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="143"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="178"/>
        <source>Do A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="148"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="179"/>
        <source>Do B</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toolbar_Editor</name>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="23"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="189"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="191"/>
        <source>Select a menu</source>
        <translation>Vybrat nabídku</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="26"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="192"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="194"/>
        <source>Lists the avaliable menus to get the corresponding actions</source>
        <translation>Uvádí dostupné nabídky pro získání odpovídajících činností</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="33"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="195"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="197"/>
        <source>Select toolbar</source>
        <translation>Vybrat nástrojový pruh</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="36"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="198"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="200"/>
        <source>List the available toolbars. The selected one will be edited</source>
        <translation>Uvádí dostupné nástrojové pruhy. Vybraný bude upraven</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="43"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="201"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="203"/>
        <source>Lists the actions in the selected toolbar</source>
        <translation>Uvádí činnosti ve vybraném nástrojovém pruhu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="52"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="204"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="206"/>
        <source>Add new Toolbar</source>
        <translation>Přidat nový nástrojový pruh</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="55"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="206"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="208"/>
        <source>New</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="67"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="208"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="210"/>
        <source>Delete selected toolbar</source>
        <translation>Smazat vybraný nástrojový pruh</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="70"/>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="173"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="210"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="236"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="212"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="238"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="84"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="212"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="214"/>
        <source>Lists the available actions for the selected menu</source>
        <translation>Uvádí dostupné činnosti pro vybranou nabídku</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="106"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="215"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="217"/>
        <source>Move Up</source>
        <translation>Posunout nahoru</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="109"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="218"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="220"/>
        <source>Moves the selected toolbar item up</source>
        <translation>Posune vybranou položku nástrojového pruhu nahoru</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="112"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="220"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="222"/>
        <source>Move &amp;Up</source>
        <translation>Posunout &amp;nahoru</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="124"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="222"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="224"/>
        <source>Move Down</source>
        <translation>Posunout dolů</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="127"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="225"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="227"/>
        <source>moves the selected toolbar item down</source>
        <translation>Posune vybranou položku nástrojového pruhu dolů</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="130"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="227"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="229"/>
        <source>Move &amp;Down</source>
        <translation>Posunout &amp;dolů</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="155"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="229"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="231"/>
        <source>Insert</source>
        <translation>Vložit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="158"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="232"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="234"/>
        <source>Adds the selected menu action to the toolbar</source>
        <translation>Přidá vybranou činnost v nabídce do nástrojového pruhu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="161"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="234"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="236"/>
        <source>&amp;Insert</source>
        <translation>&amp;Vložit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="176"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="239"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="241"/>
        <source>Removes the selected item from the active toolbar</source>
        <translation>Odstraní vybranou položku z činného nástrojového pruhu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="179"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="241"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="243"/>
        <source>&amp;Remove</source>
        <translation>&amp;Odstranit</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="204"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="243"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="245"/>
        <source>Separator</source>
        <translation>Oddělovač</translation>
    </message>
    <message>
        <source>Adds a separator after the selected toolbar item</source>
        <translation type="obsolete">Přidá oddělovač za vybranou položku v nástrojového pruhu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="207"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="245"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="247"/>
        <source>&amp;Separator</source>
        <translation>&amp;Oddělovač</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.cpp" line="155"/>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.cpp" line="175"/>
        <source>--(separator)--</source>
        <translation>--(oddělovač)--</translation>
    </message>
    <message>
        <source>custom_toolbar_%1</source>
        <translation type="vanished">vlastní_nástrojový_pruh_%1</translation>
    </message>
</context>
<context>
    <name>Wizard_Create_Plugin</name>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="14"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="186"/>
        <source>Create Plugin Wizard</source>
        <translation>Průvodce vytvořením přídavného modulu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="23"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="187"/>
        <source>Plugin Information</source>
        <translation>Informace o přídavném modulu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="29"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="188"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="39"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="189"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="46"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="190"/>
        <source>Description</source>
        <translation>Popis</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="60"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="191"/>
        <source>Script Source</source>
        <translation>Zdroj skriptu</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="70"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="192"/>
        <source>Additional Data</source>
        <translation>Dodatečná data</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="76"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="193"/>
        <source>&amp;Remove</source>
        <translation>&amp;Odstranit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="91"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="194"/>
        <source>&amp;Insert</source>
        <translation>&amp;Vložit</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="116"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="196"/>
        <source>Key</source>
        <translation>Klíč</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="121"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="198"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="130"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="199"/>
        <source>Output Directory</source>
        <translation>Výstupní adresář</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="139"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="200"/>
        <source>Browse...</source>
        <translation>Procházet...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="40"/>
        <source>Script</source>
        <translation>Skript</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="41"/>
        <source>Cusp</source>
        <translation>Špička</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="42"/>
        <source>Crossing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="73"/>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="84"/>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="115"/>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="137"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="73"/>
        <source>Name is required</source>
        <translation>Je požadován název</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="85"/>
        <source>%1 is not a writable directory.</source>
        <translation>%1 není zapisovatelný adresář.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="115"/>
        <source>Error while creating plugin file.</source>
        <translation>Chyba při vytváření přídavného modulu.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="137"/>
        <source>Error while creating source file.</source>
        <translation>Chyba při vytváření zdrojového souboru.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="158"/>
        <source>Select Directory</source>
        <translation>Vybrat adresář</translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorDialog</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.cpp" line="59"/>
        <source>Pick</source>
        <translation type="unfinished">Vybrat</translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPalette</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette.cpp" line="428"/>
        <source>Unnamed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPaletteModel</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_model.cpp" line="70"/>
        <source>Unnamed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_model.cpp" line="144"/>
        <source>%1 (%2 colors)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPaletteWidget</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="59"/>
        <source>Open a new palette from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="71"/>
        <source>Create a new palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="83"/>
        <source>Duplicate the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="121"/>
        <source>Delete the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="133"/>
        <source>Revert changes to the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="145"/>
        <source>Save changes to the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="170"/>
        <source>Add a color to the palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="182"/>
        <source>Remove the selected color from the palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="186"/>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="201"/>
        <source>New Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="187"/>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="202"/>
        <source>Name</source>
        <translation type="unfinished">Název</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="228"/>
        <source>GIMP Palettes (*.gpl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="229"/>
        <source>Palette Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="230"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="231"/>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="244"/>
        <source>Open Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="245"/>
        <source>Failed to load the palette file
%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::GradientEditor</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_editor.cpp" line="335"/>
        <source>Add Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_editor.cpp" line="344"/>
        <source>Remove Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_editor.cpp" line="352"/>
        <source>Edit Color...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::GradientListModel</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_list_model.cpp" line="231"/>
        <source>%1 (%2 colors)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::Swatch</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/swatch.cpp" line="824"/>
        <source>Clear Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/swatch.cpp" line="833"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
